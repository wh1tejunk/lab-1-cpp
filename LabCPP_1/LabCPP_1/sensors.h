#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <time.h>

using namespace std;

class Sensor
{
private:
	bool is_started = false;
	vector<int> data;
	int range_begin, range_end;
	int Interval = 1;
	string location;
public:
	Sensor(int RangeBegin, int RangeEnd) : range_begin(RangeBegin), range_end(RangeEnd) {}
	void Start() { is_started = true; }
	void Stop() { is_started = false; }
	void set_location(string _location) { location = _location; }
	void get_data(int seconds)
	{
		srand(time(NULL));
		if (is_started)
		{
			for (int i = 0; i < seconds; i++)
				if (i % Interval == 0 && i != 0)
				{
					int random = rand() % (range_end - range_begin) + range_begin;
					data.push_back(random);
				}
			if (data.size() == 0)
				cout << "There is no data yet" << endl;
			else
				for (int i = 0; i < data.size(); i++)
					cout << data[i] << endl;
		}
		else
			cout << "Timer turned off" << endl;
	}
	void get_prev_data()
	{
		if (data.size() != 0)
			for (int i = 0; i < data.size(); i++)
				cout << data[i] << endl;
		else
			cout << "There is no data yet" << endl;
	}
	void set_interval(int interval) { Interval = interval; }
	void get_interval() { cout << Interval << endl; }
	void get_status()
	{
		if (is_started)
			cout << "is on" << endl;
		else
			cout << "is off" << endl;
	}
	void get_location() { cout << location << endl; }
	void clear() { data.clear(); }
	virtual void get_sensor(int i) = 0;
};

class TermalSensor : public Sensor
{
public:
	TermalSensor() : Sensor(-100, 100) {};
	virtual void get_sensor(int i)
	{
		cout << "Sensor � " << i + 1 << " type: temperature" << endl;
		cout << "Status: ";
		this->get_status();
		cout << "Location: ";
		this->get_location();
		cout << "Interval: ";
		this->get_interval();
		cout << endl;
	}
};

class BarometerSensor : public Sensor
{
public:
	BarometerSensor() : Sensor(700, 800) {};
	virtual void get_sensor(int i)
	{
		cout << "Sensor � " << i + 1 << " type: barometer" << endl;
		cout << "Status: ";
		this->get_status();
		cout << "Location: ";
		this->get_location();
		cout << "Interval: ";
		this->get_interval();
		cout << endl;
	}
};

class WetSensor : public Sensor
{
public:
	WetSensor() : Sensor(30, 100) {};
	virtual void get_sensor(int i)
	{
		cout << "Sensor � " << i + 1 << " type: wet" << endl;
		cout << "Status: ";
		this->get_status();
		cout << "Location: ";
		this->get_location();
		cout << "Interval: ";
		this->get_interval();
		cout << endl;
	}
};

class COSensor : public Sensor
{
public:
	COSensor() : Sensor(0, 100) {};
	virtual void get_sensor(int i)
	{
		cout << "Sensor � " << i + 1 << " type: smoke" << endl;
		cout << "Status: ";
		this->get_status();
		cout << "Location: ";
		this->get_location();
		cout << "Interval: ";
		this->get_interval();
		cout << endl;
	}
};