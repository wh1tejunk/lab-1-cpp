#include <iostream>
#include <vector>
#include <string>
#include <clocale>
#include "sensors.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	vector<Sensor*> sensors;
	int selector = 1;
	int count = 0;
	while (selector)
	{
		cout << "Select an action: " << endl << endl;
		cout << "1 - install sensor" << endl;
		cout << "2 - uninstall sensor" << endl;
		cout << "3 - show sensors" << endl;
		cout << "4 - turn on sensor" << endl;
		cout << "5 - turn off sensor" << endl;
		cout << "6 - get current sensor's data" << endl;
		cout << "7 - get previous sensor's data" << endl;
		cout << "8 - clear sensor's data" << endl << endl;
		cout << "0 - exit" << endl << endl;
		cin >> selector;
		system("cls");
		switch (selector)
		{
		case 1:
		{
			int _selector;
			cout << "Select sensor's type:" << endl << endl;
			cout << "1 - temperature" << endl;
			cout << "2 - barometer" << endl;
			cout << "3 - wet" << endl;
			cout << "4 - smoke" << endl << endl;
			cin >> _selector;
			switch (_selector)
			{
				case 1:
				{
					TermalSensor* tmp = new TermalSensor;
					sensors.push_back(tmp);
					count++;
					break;
				}
				case 2:
				{
					BarometerSensor* tmp = new BarometerSensor;
					sensors.push_back(tmp);
					count++;
					break;
				}
				case 3:
				{
					WetSensor* tmp = new WetSensor;
					sensors.push_back(tmp);
					count++;
					break;
				}
				case 4:
				{
					COSensor* tmp = new COSensor;
					sensors.push_back(tmp);
					count++;
					break;
				}
				default:
				{
					cout << "Incorrect data" << endl << "Sensor wasn't successfully instlled" << endl;
					sensors.erase(sensors.begin() + count - 1);
					count--;
					break;
				}
			}
			if (sensors.size() != count || sensors.size() == 0)
				break;
			system("cls");
			int location;
			cout << "Choose a location:" << endl << endl;
			cout << "1 - kitchen" << endl;
			cout << "2 - bathroom" << endl;
			cout << "3 - living room" << endl;
			cout << "4 - sitting room" << endl << endl;
			cin >> location;
			switch (location)
			{
			case 1:
				sensors.at(count - 1)->set_location("Kitchen");
				system("cls");
				break;
			case 2:
				sensors.at(count - 1)->set_location("Bathroom");
				system("cls");
				break;
			case 3:
				sensors.at(count - 1)->set_location("Living room");
				system("cls");
				break;
			case 4:
				sensors.at(count - 1)->set_location("Sitting room");
				system("cls");
				break;
			default:
				system("cls");
				cout << "Incorrect data" << endl << "Sensor wasn't successfully instlled" << endl << endl;
				sensors.erase(sensors.begin() + count - 1);
				count--;
				system("pause");
				break;
			}
			if (sensors.size() != count || sensors.size() == 0)
				break;
			system("cls");
			cout << "Enter interval:" << endl << endl;
			int interval;
			cin >> interval;
			if (interval >= 1)
			{
				sensors.at(count - 1)->set_interval(interval);
				system("cls");
				cout << "Sensor was successfully instaled!" << endl << endl;
			}
			else
			{
				sensors.erase(sensors.begin() + count - 1);
				count--;
				system("cls");
				cout << "Value of interval < 1" << endl << "Sensor wasn't successfully instlled" << endl << endl;
			}
			break;
		}
		case 2:
		{
			int remove;
			cout << "Enter the sensor's number which you want to remove: ";
			cin >> remove;
			remove--;
			if (remove > count || remove < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				sensors.erase(sensors.begin() + remove);
				count--;
				system("cls");
				cout << "Sensor was successfully removed!" << endl << endl;
			}
			break;
		}
		case 3:
		{
			for (int i = 0; i < sensors.size(); i++)
				sensors[i]->get_sensor(i);
			system("pause");
			system("cls");
			break;
		}
		case 4:
		{
			cout << "Enter sensor's number which you want to turn on: ";
			int _selector;
			cin >> _selector;
			if (_selector > count || _selector < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				sensors[_selector - 1]->Start();
				system("cls");
				cout << "Sensor was successfully turned on!" << endl;
			}
			break;
		}
		case 5:
		{
			cout << "Enter sensor's number which you want to turn off: ";
			int _selector;
			cin >> _selector;
			if (_selector > count || _selector < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				sensors[_selector - 1]->Stop();
				system("cls");
				cout << "Sensor was successfully turned off!" << endl;
			}
			break;
		}
		case 6:
		{
			int _selector;
			cout << "Choose a sensor: ";
			cin >> _selector;
			if (_selector > count || _selector < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				cout << "How many seconds left? ";
				int seconds;
				cin >> seconds;
				if (seconds >= 0)
				{
					system("cls");
					cout << "Values:" << endl;
					sensors[_selector - 1]->get_data(seconds);
					system("pause");
				}
				else
				{
					system("cls");
					cout << "Wrong data" << endl;
				}
			}
			system("cls");
			break;
		}
		case 7:
		{
			int _selector;
			cout << "Choose a sensor: ";
			cin >> _selector;
			if (_selector > count || _selector < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				system("cls");
				cout << "Volues:" << endl;
				sensors[_selector - 1]->get_prev_data();
				system("pause");
			}
			system("cls");
			break;
		}
		case 8:
		{
			int _selector;
			cout << "Choose a sensor: ";
			cin >> _selector;
			if (_selector > count || _selector < 0)
			{
				system("cls");
				cout << "Wrong data" << endl;
			}
			else
			{
				system("cls");
				sensors[_selector - 1]->clear();
				cout << "Data was successfully cleared!" << endl;
				system("pause");
			}
			system("cls");
			break;
		}
		default:
			break;
		}
	}
	system("pause");
	return 0;
}